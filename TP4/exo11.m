addpath("../iptools")

u = imread("../images/lena.pgm");
[p, s] = perdecomp(u);
imshow(p, []);
print p.jpg
imshow(s, []);
print s.jpg

# Consistence
diff = abs(p+s-u);
printf("max of abs(p+s-u): %f %f\n", max(max(diff)));

# Version periodisees
imshow(kron(ones(2,2),u), []);
print per_u.jpg
imshow(kron(ones(2,2),p), []);
print per_p.jpg
imshow(kron(ones(2,2),s), []);
print per_s.jpg

# FFT
su = fsym2(u);
imshow(su, []);
print su.jpg
imshow(fftshift(log(1 + abs(fft2(u)))), []);
print mod_fft_u.jpg
imshow(fftshift(log(1 + abs(fft2(su)))), []);
print mod_fft_su.jpg
imshow(fftshift(log(1 + abs(fft2(p)))), []);
print mod_fft_p.jpg
imshow(fftshift(log(1 + abs(fft2(s)))), []);
print mod_fft_s.jpg
imshow(fftshift(arg(fft2(u))), []);
print arg_fft_u.jpg
imshow(fftshift(arg(fft2(su))), []);
print arg_fft_su.jpg
imshow(fftshift(arg(fft2(p))), []);
print arg_fft_p.jpg
imshow(fftshift(arg(fft2(s))), []);
print arg_fft_s.jpg