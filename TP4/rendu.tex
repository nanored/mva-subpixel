\documentclass[11pt]{article}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=2.8cm,bottom=2.9cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{tikz}
\usepackage{bbm, bm}

\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mystring}{RGB}{200,150,0}

\lstset{language=Matlab,%
	basicstyle=\color{red},
	breaklines=true,%
	morekeywords={matlab2tikz,endfunction,endif,for,endfor,break,do,until,besselj,printf},
	keywordstyle=\color{blue},%
	identifierstyle=\color{black},%
	stringstyle=\color{mystring},
	morestring=[s]{""},
	morecomment=[l][\color{mygreen}]{\#},
	showstringspaces=false,
	numbers=left,%
	numberstyle={\tiny \color{black}},% size of the numbers
	numbersep=9pt, % this defines how far the numbers are from the text
}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Imagerie sous-pixellique --- TP4
	\noindent\rule{\linewidth}{1pt}
}

\DeclareMathOperator{\sinc}{sinc}

\newcommand{\cmd}[1]{\textbf{"\texttt{#1}"}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle

	\section*{Exercice 11}
	
	\subsection*{Code}
	\lstinputlisting{exo11.m}
	\vspace{4mm}
	
	\subsection*{Question 1}
	
	La première ligne \texttt{[ny, nx] = size(u)} permet de récupérer le nombre de lignes \verb|ny| de \verb|u| et le nombre de colonnes \verb|nx|. \\
	La ligne \texttt{u = double(u)} permet de transformer une l'image de bytes en une image de doubles afin de pouvoir faire des calculs précis dans la suite. \\
	La ligne \texttt{X = 1:nx; Y = 1:ny} permet de récupérer deux vecteurs contenant les entiers entre 1 et \verb|nx| et entre 1 et \verb|ny| compris. \\
	La ligne \texttt{v = zeros(ny,nx)} permet de créer une image \verb|v| contenant uniquement des zéros et de taille $ny \times nx$. Cette image permettra de contenir l'opposé du Laplacien extérieur de \verb|u| qui est nul à l'intérieur et non nul sur les bords. Il reste alors à calculer les bonnes valeurs de \verb|v| sur les bords. \\
	La ligne \verb|v(1,X) = u(1,X)-u(ny,X)| permet de calculer la première ligne de \verb|v| sauf dans les deux coins. \\
	La ligne \verb|v(ny,X) = -v(1,X)| permet de calculer la dernière ligne de \verb|v| sauf dans les deux coins. \\
	La ligne \verb|v(Y,1) = v(Y,1)+u(Y,1)-u(Y,nx)| permet de calculer la première colonne de \verb|v|. Il est important d'ajouter \verb|v(Y,1)| afin de réutiliser les calculs partiels déjà effectués pour les deux coins. Après cette opération les calculs des deux coins de gauches sont alors finis. \\
	La ligne \verb|v(Y,nx) = v(Y,nx)-u(Y,1)+u(Y,nx)| permet de calculer la dernière colonne de \verb|v|. \\
	La ligne \texttt{fx = repmat(cos(2.*pi*(X -1)/nx),ny,1)} permet de calculer la partie oscillante selon l'axe $x$ de la transformée de Fourier de l'opérateur Laplacien pour une image de taille $ny \times nx$ à un facteur $1/2$ près. C'est à dire la transformée de Fourier de l'image :
	$$ \phi_x(x, y) = \syst{ll}{
		1/2 & \text{Si } |x| = 1 \text{ et } y = 0 \\
		0 & \text{Sinon}
	} $$
	La ligne \texttt{fy = repmat(cos(2.*pi*(Y’-1)/ny),1,nx)} permet de calculer la partie oscillante selon l'axe $y$ de la transformée de Fourier de l'opérateur Laplacien pour une image de taille $ny \times nx$ à un facteur $1/2$ près. C'est à dire la transformée de Fourier de l'image :
	$$ \phi_y(x, y) = \syst{ll}{
		1/2 & \text{Si } |y| = 1 \text{ et } x = 0 \\
		0 & \text{Sinon}
	} $$
	La ligne \texttt{fx(1,1)=0.} permet d'éviter une division par zéro en 0 à la ligne suivante. Comme \verb|v| est de moyenne nulle, sa transformée de Fourier et nulle en zéro et donc on peut donner à \verb|fx(1,1)| n'importe quelle valeur différente de 1. \\
	La ligne \texttt{s = real(ifft2(fft2(v)*0.5./(2.-fx-fy)))} permet de calculer la composante smooth de l'image \verb|u| selon la formule vue en cours. En effet \verb|2*(2-fx-fy)| correspond bien à la transformée de Fourier de l'opposé de l'opérateur Laplacien $4 \delta_0 - 2 \phi_x - 2 \phi_y = - \Delta$. \\
	Finalement la ligne \verb|p = u-s| donne simplement la composante périodique de l'image \verb|u|.
	
	\subsection*{Question 2}
	
	On prend pour l'image $u$, l'image de Lena :
	\begin{figure}[h]
		\centering
		\includegraphics[width=3.5cm]{lena.jpg}
	\end{figure}

	Pour afficher les composantes, il est important d'ajouter le paramètre \verb|[]| à \verb|imshow| afin de redéfinir la valeur correspondant au blanc et celle correspondant au noir puisque par défaut ces paramètres valent 1 et 0, alors que notre image prend des valeurs dans $[0, 255]$. On obtient ensuite les deux composantes suivantes :
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}
			\node (a) at (0, 0) {\includegraphics[width=4.5cm]{p.jpg}};
			\node[below=1.3cm] at (a) {Périodique};
			\node (a) at (5, 0) {\includegraphics[width=4.5cm]{s.jpg}};
			\node[below=1.3cm] at (a) {Lisse};
		\end{tikzpicture}
	\end{figure}

	On vérifie la consistance de la décomposition en affichant le maximum de l'image \verb|abs(p+s-u)| et on obtient bien 0. On visualise aussi la périodicité de \verb|p| en affichant les périodisées des images :
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}
		\node (a) at (0, 0) {\includegraphics[width=5cm]{per_u.jpg}};
		\node[below=1.45cm] at (a) {Lena};
		\node (a) at (5, 0) {\includegraphics[width=5cm]{per_p.jpg}};
		\node[below=1.45cm] at (a) {Périodique};
		\node (a) at (10, 0) {\includegraphics[width=5cm]{per_s.jpg}};
		\node[below=1.45cm] at (a) {Lisse};
		\end{tikzpicture}
	\end{figure}

	Finalement on peut observer les log-modules et arguments des transformées de Fourier de \verb|u|, \verb|p|, \verb|s| et \verb|fsym2(u)| comme suggéré dans l'énoncé. Dans le domaine initial la principale différence que l'on observe entre \verb|p| et \verb|u| est le fait que sur les bords \verb|p| semble être plus lisse et le niveau de gris peut aussi être plus élevé ou plus faible. Pour le côté plus lisse, cela peut se justifier en constatant que l'image \verb|s| contient des détails sur les bords et soustraire des détails à \verb|u| revient à lisser \verb|u|. Les changement de niveau de gris plus élevés sur les bords se justifie par le fait que les intensités les plus élevés dans \verb|s| se trouvent sur les bords. En effet le Laplacien de \verb|s| est nulle à l'intérieur et donc les extremas sont sur les bords. Dans le domaine de Fourier la différence se trouve dans la disparition de la croix due à la périodicité forcée aussi bien pour le module que pour l'argument.
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}
		\node (a) at (0, 0) {\includegraphics[width=4.6cm]{mod_fft_u.jpg}};
		\node[below=1.45cm] at (a) {log-module fft u};
		\node (a) at (3.75, 0) {\includegraphics[width=4.6cm]{mod_fft_p.jpg}};
		\node[below=1.45cm] at (a) {log-module fft p};
		\node (a) at (7.5, 0) {\includegraphics[width=4.6cm]{arg_fft_u.jpg}};
		\node[below=1.45cm] at (a) {argument fft u};
		\node (a) at (11.25, 0) {\includegraphics[width=4.6cm]{arg_fft_p.jpg}};
		\node[below=1.45cm] at (a) {argument fft p};
		\end{tikzpicture}
	\end{figure}

	Enfin ce traitement est plus intéressant que la symétrisation car on est pas obligé d'agrandir le domaine et parce qu'on ne force pas de symétrisation ou d'anti-symétrisation dans la transformée de Fourier comme on peut le constater dans la figure suivante :
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}
		\node (a) at (0, 0) {\includegraphics[width=4.8cm]{mod_fft_su.jpg}};
		\node[below=1.45cm] at (a) {log-module fft \texttt{fsym2(u)}};
		\node (a) at (5.5, 0) {\includegraphics[width=4.8cm]{arg_fft_su.jpg}};
		\node[below=1.45cm] at (a) {argument fft \texttt{fsym2(u)}};
		\end{tikzpicture}
	\end{figure}
	
	\section*{Exercice 13}
	
	\subsection*{Question 1}
	
	On exprime $v$ en fonction de $u$ via la relation :
	$$ v(n) = \dfrac{1}{2} \left( u(n) + u(n+1) \right) $$
	On utilise ensuite la linéarité de la transformée de Fourier pour avoir :
	$$ \hat{v}(\xi) = \dfrac{1}{2} \left( \hat{u}(\xi) + \sum_{n \in \Z} u(n+1) e^{-in\xi} \right) $$
	Avec un changement de variable on a :
	$$ \sum_{n \in \Z} u(n+1) e^{-in\xi} = \sum_{n \in \Z} u(n) e^{-i(n-1)\xi} = e^{i\xi} \hat{u}(\xi) $$
	D'où :
	\begin{center}
		\fbox{$ \displaystyle \hat{v}(\xi) = \dfrac{1}{2} \left( 1 + e^{i\xi} \right) \hat{u}(\xi) = e^{i \frac{1}{2} \xi} \cos \left( \frac{1}{2} \xi \right) \hat{u}(\xi) $}
	\end{center}

	Ensuite on cherche une expression de $w$. On pose $U$ l'interpolation de Shannon :
	$$ U = u * \sinc \qquad \hat{U} = \dfrac{1}{2 \pi} \hat{u} \, . \, \mathbbm{1}_{[-\pi, \pi]} $$
	Ensuite il nous faut translaté $U$ de $1/2$ et multiplier par un peigne afin de revenir en discret :
	$$ w = \left( U * \delta_{-\frac{1}{2}} \right) . \Pi_1 $$
	On obtient la transformée de Fourier suivante :
	$$ \hat{w} = \left( \dfrac{1}{2 \pi} \hat{u} \, . \, \mathbbm{1}_{[-\pi, \pi]} \, . \, e^{\frac{1}{2} i \cdot} \right) * (2 \pi) \Pi_{2 \pi} $$
	On évalue ensuite $\hat{w}$ en $\xi$ :
	$$ \hat{w}(\xi) = \int_{-\pi}^{\pi} \hat{u}(t) e^{\frac{1}{2} i t} \Pi_{2 \pi}(\xi - t) dt = \hat{u}(t_0) e^{\frac{1}{2} i t_0} $$
	Où $t_0 \equiv \xi \, [2 \pi]$ et $t_0 \in [-\pi, \pi]$. Comme $\hat{u}$ est $2\pi$-périodique, on a $\hat{u}(t_0) = \hat{u}(\xi)$ et donc :
	$$ \hat{w}(\xi) = \hat{u}(\xi) e^{\frac{1}{2} i \xi} e^{\frac{1}{2} i (t_0 - \xi)} = \syst{ll}{
		\hat{u}(\xi) e^{\frac{1}{2} i \xi} & \text{Si } \cos \left( \frac{1}{2} \xi \right) \geqslant 0 \\
		- \hat{u}(\xi) e^{\frac{1}{2} i \xi} & \text{Sinon}
	} $$
	Ce qui nous permet d'écrire :
	\begin{center}
		\fbox{$ \displaystyle \hat{v}(\xi) = \left| \cos \left( \frac{1}{2} \xi \right) \right| \hat{w}(\xi) $}
	\end{center}

	\subsection*{Question 2}
	
	On exprime $v$ en fonction de $u$ via la relation :
	$$ v(n) = \left\{ \begin{array}{ll}
		u(n/2) & \text{Si } n \equiv 0 \, [2] \\
		\dfrac{1}{2} \left( u((n-1)/2) + u((n+1)/2) \right) & \text{Si } n \equiv 1 \, [2]
	\end{array} \right. $$
	Finalement on obtient :
	$$ \hat{v}(\xi) = \sum_{n \in \Z} v(n) e^{-in \xi} = \sum_{n \in \Z} u(n) e^{-2in \xi} + \dfrac{1}{2} \sum_{n \in \Z} u(n) e^{-i (2n+1) \xi} + \dfrac{1}{2} \sum_{n \in \Z} u(n) e^{-i (2n-1) \xi} $$
	En regroupant les 3 sommes on obtient :
	$$ \hat{v}(\xi) = \sum_{n \in \Z} u(n) e^{-in (2\xi)} \left( 1 + \dfrac{1}{2} e^{-i \xi} + \dfrac{1}{2} e^{i \xi} \right) = \hat{u}(2 \xi) (1 + \cos(\xi)) $$
	Ainsi :
	\begin{center}
		\fbox{$ \displaystyle \hat{v}(\xi) = \hat{u}(2 \xi) (1 + \cos(\xi))$}
	\end{center}
	
	Cette fois-ci $w$ peut être exprimer de la manière suivante :
	$$ w = U \left( \dfrac{1}{2} \, \cdot \right) \, . \, \Pi_1 $$
	On calcule ensuite la transformée de Fourier (en utilisant $\mathbbm{1}_{[-\pi, \pi]}(2x) = \mathbbm{1}_{[-\pi/2, \pi/2]}(x)$) :
	$$ \hat{w} = 2 \left( \hat{u}(2 \, \cdot) . \mathbbm{1}_{[-\pi/2, \pi/2]} \right) * \Pi_{2 \pi} $$
	A nouveau grâce à la $2\pi$-périodicité on abouti à :
	\begin{center}
		\fbox{$ \displaystyle \hat{w}(\xi) = \hat{u}(2 \xi) \left( \mathbbm{1}_{[-\pi/2, \pi/2]} * \Pi_{2 \pi} \right)(\xi) = \syst{ll}{
				2 \hat{u}(2 \xi) & \text{Si } \cos(\xi) \geqslant 0 \\
				0 & \text{Sinon}
			} $}
	\end{center}
	
	Ainsi $\hat{v}$ et $\hat{w}$ peuvent s'exprimer comme $\hat{u}(2 \cdot)$ fois une autre fonction. Pour $\hat{v}$ cette fonction est lisse et pour $\hat{w}$ cette fonction est constante par morceaux. On remarque aussi que ces deux fonction ont une moyenne égale à 1.
	
	\begin{center}
		\begin{tikzpicture}[scale=1.2]
			\draw[->] (-4, 0) -- (4, 0);
			\draw[->] (0, -0.5) -- (0, 2.5);
			\draw[blue, very thick, samples=100, domain=-3.5:3.5, variable=\x] plot (\x, {1 + cos(\x * 180)}) node[right] {$1 + \cos$};
			\draw[red, very thick] (-0.5, 2) -- (0.5, 2);
			\draw[red, very thick] (1.5, 2) -- (2.5, 2);
			\draw[red, very thick] (-1.5, 2) -- (-2.5, 2);
			\draw[red, very thick] (-0.5, 0) -- (-1.5, 0);
			\draw[red, very thick] (-2.5, 0) -- (-3.5, 0);
			\draw[red, very thick] (0.5, 0) -- (1.5, 0);
			\draw[red, very thick] (2.5, 0) -- (3.5, 0) node[below] {$\mathbbm{1}_{[-\pi/2, \pi/2]} * \Pi_{2 \pi}$};
		\end{tikzpicture}
	\end{center}
	
\end{document}