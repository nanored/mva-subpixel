addpath("../iptools")

n = 3;
u = double(imread("../images/crop_bouc.pgm"));
imshow(u, []);
print bouc.jpg
imshow(log(1 + abs(fftshift(fft2(u)))), []);
print boucfft.jpg
v = fzoom(u, 16, n);
imshow(v, []);
print boucf.jpg

u = imread("../images/crop_cameraman.pgm");
imshow(u, []);
print cam.jpg
imshow(log(1 + abs(fftshift(fft2(u)))), []);
print camfft.jpg
v = fzoom(u, 16, n);
imshow(v, []);
print camf.jpg