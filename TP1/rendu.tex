\documentclass[11pt]{article}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=2.8cm,bottom=2.9cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mystring}{RGB}{200,150,0}

\lstset{language=Matlab,%
	basicstyle=\color{red},
	breaklines=true,%
	morekeywords={matlab2tikz,endfunction,endif,for,endfor,break,do,until,besselj,printf},
	keywordstyle=\color{blue},%
	identifierstyle=\color{black},%
	stringstyle=\color{mystring},
	morestring=[s]{""},
	morecomment=[l][\color{mygreen}]{\#},
	showstringspaces=false,
	numbers=left,%
	numberstyle={\tiny \color{black}},% size of the numbers
	numbersep=9pt, % this defines how far the numbers are from the text
}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Imagerie sous-pixellique --- TP1
	\noindent\rule{\linewidth}{1pt}
}

\newcommand{\cmd}[1]{\textbf{"\texttt{#1}"}}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section*{Code}
	\lstinputlisting{tp1.m}
	
	\section*{Question 1}
	
	On obtient le profil du noyau de diffraction suivant :
	\begin{figure}[h]
		\centering
		\includegraphics[scale=0.35]{fig11.jpg}
		\includegraphics[scale=0.35]{fig12.jpg}
		\caption{A gauche la fonction $f$ du profil est affiché. A droite la fonction $\min(f, 0.1)$ est affiché afin de mieux voir les rebonds}
	\end{figure}

	\section*{Question 2}
	
	En passant en 2 dimensions, voici la tâche d'Airy observé :
	\begin{figure}[h]
		\centering
		\includegraphics[scale=0.35]{fig21.jpg}
		\includegraphics[scale=0.35]{fig22.jpg}
		\caption{A gauche la tâche d'Airy est affiché. A droite la tâche est affiché avec de la saturation afin de mieux voir les rebonds}
	\end{figure}

	\section*{Question 3}
	
	On calcule alors les transformées de Fourier du profil radial et de la tâche en dimension 2 :
	\begin{figure}[h]
	\centering
	\includegraphics[scale=0.35]{fig31.jpg}
	\includegraphics[scale=0.35]{fig32.jpg}
	\caption{A gauche le module de la transformée de Fourier discrète du profil radial sur $]0, 10]$ et à droite le logarithme du module de la transformée de Fourier discrète de la tâche d'Airy sur $[-100, 100]$}
	\end{figure}
	
	\section*{Question 4}
	
	La droite qui joint les deux centres des tâches admet deux maximums si et seulement si l'intensité se remet à croître après avec décroît sur le segment joignant les deux centres des tâches. La portion de code de la ligne 41 à la ligne 65 utilise cette caractérisation pour trouver la distance critique à laquelle deux tâches d'Airy deviennent indiscernables. On obtient alors la réponse :
	\begin{center}
		\fbox{La distance critique est : $\simeq 0.78$ rayons d'Airy}
	\end{center}

	\section*{Question 5}
	
	On calcule le noyau de diffraction associé à une pupille de télescope (disque de diamètre $D$ occulté au centre par un disque de diamètre $\epsilon D$).
	$$ \begin{array}{lll}
	K_{diff}(x) & = & \displaystyle \dfrac{1}{2 f^2} \left| \int_{\frac{\epsilon D}{2}}^{\frac{D}{2}} \int_{0}^{2 \pi} \exp \left( \dfrac{2 i \pi}{\lambda f} | x | \rho \sin \theta \right) \rho d \theta d \rho \right|^2 \\[5mm]
	& = & \displaystyle \dfrac{1}{2 f^2} \left| 2 \int_{\frac{\epsilon D}{2}}^{\frac{D}{2}} \int_{0}^{\pi} \cos \left( \dfrac{2 \pi}{\lambda f} | x | \rho \sin \theta \right) \rho d \theta d \rho \right|^2
	\end{array} $$
	On pose alors $r = \dfrac{\pi D |x|}{\lambda f}$ et $\rho' = \dfrac{2r}{D} \rho = \dfrac{2 \pi |x|}{\lambda f} \rho$.
	$$ \begin{array}{lll}
	K_{diff}(x) & = & \displaystyle \dfrac{1}{2 f^2} \left| \dfrac{D^2}{2 r^2} \int_{\epsilon r}^{r} \int_{0}^{\pi} \cos \left( \rho' \sin \theta \right) \rho' d \theta d \rho' \right|^2 \\[5mm]
	& = & \displaystyle \dfrac{1}{2 f^2} \left| \dfrac{\pi D^2}{2 r^2} \int_{\epsilon r}^{r} J_0(\rho') \rho' d \rho' \right|^2 \\[5mm]
	& = & \displaystyle \dfrac{1}{2 f^2} \left| \dfrac{\pi D^2}{2 r^2} \left(r J_1(r) - \epsilon r J_1(\epsilon r) \right) \right|^2
	\end{array} $$
	On obtient finalement :
	\begin{center}
		\fbox{$\displaystyle K_{diff}(x) = C \left( \dfrac{2 J_1(r)}{r} - \dfrac{2 \epsilon J_1(\epsilon r)}{r} \right)^2 \qquad \text{avec } C = \dfrac{\pi^2 D^4}{32 f^2}, \, r = \dfrac{\pi D |x|}{\lambda f}$}
	\end{center}

	\section*{Question 6}
	
	Les noyaux de la lunette et du télescope ont la même constante multiplicative. On peut alors comparer leurs FTM avec le morceau de code de la ligne 68 à 74.
	\begin{figure}[h]
		\centering
		\includegraphics[scale=0.4]{fig6.jpg}
		\caption{La courbe rouge est la FTM obtenue pour le profil radial du télescope et la courbe en bleu est obtenue pour la lunette}
	\end{figure}
	
	\fbox{\parbox{0.92\linewidth}{On observe que les deux FTM ont le même domaine. On peut alors en déduire que la résolution d'une lunette et d'un télescope est identique.}}
	
\end{document}