#!/usr/bin/octave

### Exercice 1 ###

# Question 1
x = linspace(0.0001, 10, 500);
function res = F(r)
  res = (2 * besselj(1, r) ./ r).^2;
endfunction
f = F(x);
plot(x, f);
title("Sans saturation");
print fig11.jpg
plot(x, min(f, 0.1));
title("Avec saturation");
print fig12.jpg

# Question 2
x2 = linspace(-10, 10, 500);
[X, Y] = meshgrid(x2, x2);
Z = F(sqrt(X.^2 + Y.^2));
imshow(Z);
title("Sans saturation");
print fig21.jpg
imshow(Z, [0, 0.02]);
title("Avec saturation");
print fig22.jpg

# Question 3
ff = abs(fft(f));
plot(x, fftshift(ff));
print fig31.jpg

x2 = linspace(-100, 100, 500);
[X, Y] = meshgrid(x2, x2);
Z = F(sqrt(X.^2 + Y.^2));
fZ = log(1 + abs(fft2(Z)));
imshow(fftshift(fZ), [0, max(max(fZ))]);
print fig32.jpg

# Question 4
d = 2.5;
step = 0.004;
sep = false;
do
  x0 = linspace(0.0001, d-0.0001, 100);
  y = abs(x0 .- d);
  g = F(x0) .+ F(y);
  decr = false; # Si g a deja decroit avant i
  for i = 1:99
    if (g(i+1) > g(i))
      # Si g a deja ete decroissante et qu'elle recroit a nouveau
      if decr
        # Alors les sommets sont separes
        sep = true;
        break;
      endif
    else
      decr = true;
    endif
  endfor
  d += step;
until sep
prop = (d-step/2) / (1.22*pi);  
printf("la distance critique est : %f rayons d'Airy\n", prop);

# Question 6
epsilon = 1/4;
g = (2 * (besselj(1, x) .- epsilon * besselj(1, epsilon*x)) ./ x).^2;
fg = abs(fft(g))
plot(x, fftshift(ff), "b");
hold on;
plot(x, fftshift(fg), "r");
print fig6.jpg
