# Exercice 4

# Question 1

u = double(imread("../images/room.pgm"))/255;
lambda = 3;
v = u(1:lambda:end,1:lambda:end);
w = kron(v,ones(lambda));
[ny,nx] = size(u);
imshow2([u, w(1:ny,1:nx)]);
imwrite(w(1:ny,1:nx), "aliasing.jpg");

# Question 3

f = zeros(512);
f(190,50) = 2;
onde = real(ifft2(f));
imshow(onde,[]);
ft = abs(fft2(onde));
imshow(fftshift(ft), "xdata", -256:255, "ydata", -256:255); axis on

onde2 = onde(1:2:end,1:2:end);
ft2 = abs(fft2(onde2));
imshow(onde2, []);
imshow(fftshift(ft2), [], "xdata", -256:255, "ydata", -256:255); axis on
[m,pos] = max(ft2(:));
[x,y] = ind2sub(size(ft2), pos);
if x >= 128
  x -= 256;
endif
if y >= 128
  y -= 256;
endif
printf("Les coordonnées du pic dans la première image sont (49, 189),\n");
printf("tandis que dans la seconde les coordonnées sont (%d, %d)\n", y-1, x-1);