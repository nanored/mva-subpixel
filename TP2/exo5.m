## Exercice 5 ##

# Question 1
f = zeros(64);
f(18,2)=2;
onde = real(ifft2(f));
imshow(onde, []);
title("Onde pure");
print onde_pure.jpg
imshow(onde.^2, []);
title("Carre de l'onde");
print onde_carre.jpg
imshow(fftshift(abs(fft2(onde))), [], "xdata", -32:31, "ydata", -32:31);

# Question 2
onde2 = fftzoom(onde, 2);
imshow(onde2, []);
title('Onde pure avec fftzoom', []);
print onde_pure2.jpg
imshow(onde2.^2, [])
title('Onde au carre avec fftzoom', []);
print onde_carre2.jp

# Question 3
function v = gradn(u)
  smallU = u(1:end-1, 1:end-1);
  rightU = u(2:end, 1:end-1);
  topU = u(1:end-1, 2:end);
  v = sqrt((rightU - smallU).^2 + (topU - smallU).^2);
endfunction
nimes = double(imread('../images/nimes.pgm'))/255;
dnimes = gradn(nimes);
axis on;
imshow(dnimes, []);
title("Gradient de l'image de Nimes");
print dnimes.jpg
imshow(dnimes(200:260, 260:300), [], "xdata", 260:300, "ydata", 200:260);
title("Zoom sur le gradient de l'images de Nimes")
print dnimes_zoom.jpg

nimes2 = fftzoom(nimes, 2);
imshow(gradn(nimes2)(400:520, 520:600), [], "xdata", 260:300, "ydata", 200:260);
title("Zoom sur le gradient de l'images de Nimes apres fftzoom")
print dnimes_zoom2.jpg